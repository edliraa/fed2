import Post from "../../components/Post/Post";
import posts from "../../components/posts";

const Home = () => {
  return (
    <>
      <div className="row">
        <div className="col-lg-12 p-3">
          <h4 className="font-weight-bold">Home</h4>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
      </div>
      <div className="row">
        {posts.map((el, index) => {
          return (
            <div className="col-lg-6">
              <Post
                img={el.img}
                title={el.title}
                body={el.body}
                link={el.Link}
              />
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Home;
